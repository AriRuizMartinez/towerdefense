using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GoldScript : MonoBehaviour
{
    //[SerializeField] private GameObject bala;
    [SerializeField] private InformacionJugador player;
    private int money = 50;
    private void Awake()
    {
        player.money = money;
    }
    public void MoreGold(int mon)
    {
        money = player.money;
        money += mon;
        player.money = money;
        this.gameObject.GetComponent<TextMeshProUGUI>().text = "Gold: "+money;
    }
    public void LessGold(int mon)
    {
        money = player.money;
        money -= mon;
        player.money = money;
        this.gameObject.GetComponent<TextMeshProUGUI>().text = "Gold: " + money;
    }
}
