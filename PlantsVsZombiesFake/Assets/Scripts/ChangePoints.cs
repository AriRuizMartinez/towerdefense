using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ChangePoints : MonoBehaviour
{
    public InformacionJugador points;
    void Awake()
    {
        int puntos = points.points;
        this.gameObject.GetComponent<TextMeshProUGUI>().text = "Points: "+puntos;
    }

}
