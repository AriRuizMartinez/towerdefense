using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SumPoints : MonoBehaviour
{
    private int points = 0;
    [SerializeField] InformacionJugador player;
    private void Awake()
    {
        StartCoroutine(MorePoints());
    }
    private IEnumerator MorePoints()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.1f);
            points += 10;
            player.points = points;
            this.gameObject.GetComponent<TextMeshProUGUI>().text = "Points: " + points;
        }
    }
}
