using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class InformacionJugador : ScriptableObject
{
    public int money;
    public int points;
    public int vidaBase;
}
