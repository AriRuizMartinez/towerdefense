using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemigoController : MonoBehaviour
{
    private int atk;
    public int hp;
    private int vel;
    public bool air;
    private Sprite[] sprite;
    private float scale;
    public int money;
    private TiposEnemigos type;
    public InformacionJugador player;
    private Transform[] waypoints;
    private int numWaypoint = 0;
    private float m_SQDistancePerFrame;
    private SpriteRenderer m_SpriteRenderer;
    private Rigidbody2D m_RigidBody2D;

    public void Awake()
    {
    }
    public int getNumWaypoint()
    {
        return numWaypoint;
    }
    public void Init(TiposEnemigos type, Transform[] waypoints)
    {
        this.type = type;
        this.waypoints = waypoints;
        atk = type.atk;
        hp = type.hp;
        vel = type.vel;
        sprite = type.sprite;
        scale = type.scale;
        money = type.money;
        air = type.air;
        numWaypoint = 0;

        m_RigidBody2D = this.GetComponent<Rigidbody2D>();
        m_RigidBody2D.velocity = (Vector2) ((waypoints[numWaypoint].position - this.transform.position).normalized * vel);

        m_SpriteRenderer = this.GetComponent<SpriteRenderer>();
        m_SpriteRenderer.sprite = type.sprite[0];
        m_SpriteRenderer.flipX = false;
        this.transform.localScale = new Vector3(scale, scale, 1);

        m_SQDistancePerFrame = vel * Time.fixedDeltaTime;
        m_SQDistancePerFrame *= m_SQDistancePerFrame;
    }

    /*
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.tag == "WayPoint")
        {
            numWaypoint++;
            if (waypoints.Length <= numWaypoint)
            {
                player.vidaBase -= atk;
                this.gameObject.SetActive(false);
                if (player.vidaBase < 0) SceneManager.LoadScene("MainMenu");
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = (waypoints[numWaypoint].position - this.transform.position).normalized * vel;
            }            
        }
    }*/
    

    private void FixedUpdate()
    {
        //Debug.Log(Vector3.SqrMagnitude(waypoints[numWaypoint].position - this.transform.position));
        if (Vector3.SqrMagnitude(waypoints[numWaypoint].position - this.transform.position) <= m_SQDistancePerFrame)
        {
            numWaypoint++;
            if (waypoints.Length == numWaypoint)
            {
                player.vidaBase -= atk;
                this.gameObject.SetActive(false);
                if (player.vidaBase < 0) SceneManager.LoadScene("MainMenu");
            }
            else
            {
                m_RigidBody2D.velocity = (waypoints[numWaypoint].position - this.transform.position).normalized * vel;
                if(Mathf.Abs(m_RigidBody2D.velocity.x) < Mathf.Abs(m_RigidBody2D.velocity.y))
                {
                    if (m_RigidBody2D.velocity.y < 0)
                    {
                        m_SpriteRenderer.sprite = type.sprite[2];
                        m_SpriteRenderer.flipX = false;
                    }
                    else
                    {
                        m_SpriteRenderer.sprite = type.sprite[1];
                        m_SpriteRenderer.flipX = false;
                    }

                }
                else
                {
                    if (m_RigidBody2D.velocity.x < 0)
                    {
                        m_SpriteRenderer.sprite = type.sprite[0];
                        m_SpriteRenderer.flipX = true;
                    }
                    else
                    {
                        m_SpriteRenderer.sprite = type.sprite[0];
                        m_SpriteRenderer.flipX = false;
                    }
                    
                }
            }
        }
    }
}
