using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Prueba : MonoBehaviour
{
    [SerializeField]
    private InputActionAsset m_InputAsset;
    private InputActionAsset m_Input;
    [SerializeField] private TorretaController[] m_Controller;
    [SerializeField]GameEvent m_GameEventSpeed;
    [SerializeField]GameEvent m_GameEventAttack;
    [SerializeField] private InformacionJugador player;
    [SerializeField] private GoldScript gold;


    void Start()
    {
        m_Input = Instantiate(m_InputAsset);
        m_Input.FindActionMap("Default").FindAction("IncrAtk").performed += AttackIncrement;
        m_Input.FindActionMap("Default").FindAction("IncrSpd").performed += SpeedIncrement;

        m_Input.FindActionMap("Default").Enable();
    }

    public void AttackIncrement(InputAction.CallbackContext context)
    {
        /*foreach (TorretaController controller in m_Controller) 
        {
            StartCoroutine(controller.MoreAttack());
        }*/
        if (player.money >= 1000)
        {
            gold.LessGold(1000);
            m_GameEventAttack?.Raise();
        }
    }

    public void SpeedIncrement(InputAction.CallbackContext context)
    {
        /*foreach (TorretaController controller in m_Controller)
        {
            StartCoroutine(controller.MoreSpeed());
        }*/
        if (player.money >= 1000)
        {
            gold.LessGold(1000);
            m_GameEventSpeed?.Raise();
        }
    }
}
