using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    public static SpawnerController instance;
    private List<GameObject> pooled = new List<GameObject>();
    private int amountPool = 50;
    private float seg = 5;
    private int max = 1;

    [SerializeField] private GameObject Enemy;
    [SerializeField] private TiposEnemigos[] Type;
    [SerializeField] private InformacionJugador player;
    [SerializeField] private Transform[] WayPoints;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    void Start()
    {
        for (int i = 0; i < amountPool; i++)
        {
            GameObject obj = Instantiate(Enemy,transform);
            obj.SetActive(false);
            pooled.Add(obj);
        }
        StartCoroutine(Spawnear());
        StartCoroutine(TiposEnemigo());
        StartCoroutine(MenosSegs());
        player.vidaBase = 50;
    }

    public GameObject GetPooled()
    {
        for (int i = 0; i < pooled.Count; i++)
        {
            if (!pooled[i].activeInHierarchy)
            {
                return pooled[i];
            }
        }
        return null;
    }
    private IEnumerator Spawnear()
    {
        while(true)
        {
            yield return new WaitForSeconds(seg);
            GameObject obj = GetPooled();
            if (obj != null)
            {
                int random = Random.Range(0, max);
                obj.SetActive(true);
                obj.GetComponent<Transform>().position = this.gameObject.GetComponent<Transform>().position;
                obj.GetComponent<EnemigoController>().Init(Type[random], WayPoints);
            }
        }
    }
    private IEnumerator TiposEnemigo()
    {
        while (true)
        {
            yield return new WaitForSeconds(15);
            max++;
            if (max == Type.Length) yield break;
        }
    }
    private IEnumerator MenosSegs()
    {
        while (true)
        {
            yield return new WaitForSeconds(15);
            seg -= 0.25f;
            if(seg < 0.5) yield break;
        }
    }
}
