using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody2D))]
public class BalaController : MonoBehaviour
{
    public GameObject e;
    public int atk;
    private Vector3 v;
    //public delegate void Morir(int n);
    //public event Morir OnMorir;
    [SerializeField]
    private GameEventInt OnMorir;
    private void FixedUpdate()
    {
        v = (e.transform.position - this.gameObject.transform.position).normalized;
        this.gameObject.GetComponent<Rigidbody2D>().velocity = v * 10;
        if(!e.activeSelf) this.gameObject.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject == e)
        {
            this.gameObject.SetActive(false);
            EnemigoController e = collision.gameObject.GetComponent<EnemigoController>();
            e.hp -= atk;
            if(e.hp <= 0)
            {
                if (OnMorir != null) OnMorir.Raise(e.money);
                e.gameObject.SetActive(false);
            }
        }
    }
}
