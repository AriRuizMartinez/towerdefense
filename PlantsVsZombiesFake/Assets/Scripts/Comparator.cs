using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Comparator : IComparer<EnemigoController>
{
    public int Compare(EnemigoController x, EnemigoController y)
    {

        return y.getNumWaypoint().CompareTo(x.getNumWaypoint());
    }
}

