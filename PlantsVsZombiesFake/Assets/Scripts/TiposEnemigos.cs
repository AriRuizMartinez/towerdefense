using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TiposEnemigos : ScriptableObject
{
    public int atk;
    public int hp;
    public int vel;
    public bool air;
    public Sprite[] sprite;
    public float scale;
    public int money;
    public Vector3[] waypoints;
}
