using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TiposDefensa : ScriptableObject
{
    public int atk;
    public int dist;
    public int cost;
    public float vel;
    public bool air;
    public float scale;
}
