using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
    private List<GameObject> pooled = new List<GameObject>();
    private int amountPool = 8;
    [SerializeField] private GameObject Tower;
    [SerializeField] private Sprite sprite;
    [SerializeField] private GameObject container;
    [SerializeField] private InformacionJugador player;
    [SerializeField] private GameEventInt OnCrear;
    private Vector3 initialPosition;
    void Start()
    {
        initialPosition = transform.position;
        for (int i = 0; i < amountPool; i++)
        {
            GameObject obj = Instantiate(Tower, container.transform);
            obj.SetActive(false);
            pooled.Add(obj);
        }
    }

    public GameObject GetPooled()
    {
        for (int i = 0; i < pooled.Count; i++)
        {
            if (!pooled[i].activeInHierarchy)
            {
                return pooled[i];
            }
        }
        return null;
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit2D hit = Physics2D.Raycast(cameraRay.origin, cameraRay.direction, Mathf.Infinity);

        if (hit.rigidbody != null)
        {
            //Debug.Log(string.Format("Hem tocat {0}", hit.rigidbody.name));
            GameObject g = hit.rigidbody.gameObject;
            if (g.GetComponent<isEmpty>() != null) 
            {
                if (g.GetComponent<isEmpty>().empty&&this.gameObject.GetComponent<TorretaController>().tp.cost<=player.money)
                {
                    GameObject obj = GetPooled();
                    if (obj != null)
                    {
                        OnCrear?.Raise(this.gameObject.GetComponent<TorretaController>().tp.cost);
                        //player.money -= this.gameObject.GetComponent<TorretaController>().tp.cost;
                        g.GetComponent<isEmpty>().empty = false;
                        obj.AddComponent<SpriteRenderer>();
                        obj.GetComponent<SpriteRenderer>().sprite = sprite;

                        obj.GetComponent<Transform>().position = g.GetComponent<Transform>().position;
                        obj.transform.localScale = new Vector3(this.gameObject.GetComponent<TorretaController>().tp.scale, this.gameObject.GetComponent<TorretaController>().tp.scale, 1);
                        if(this.gameObject.GetComponent<TorretaController>().tp.scale == 0.55f)
                        {
                            obj.GetComponent<SpriteRenderer>().flipX = true;
                        }
                        Destroy(obj.GetComponent<DragHandler>());
                        obj.SetActive(true);
                    }
                }
            }
        }
        else
        {
            //Debug.Log("No hem tocat res");
        }
        transform.position = initialPosition;
    }
}
