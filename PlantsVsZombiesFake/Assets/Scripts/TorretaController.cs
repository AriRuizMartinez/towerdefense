using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class TorretaController : MonoBehaviour
{
    public int atk;
    public int dist;
    public int cost;
    public float vel;
    public bool air;
    public TiposDefensa tp;
    private List<EnemigoController> enemigos = new List<EnemigoController>();
    private Comparator c;
    private List<GameObject> pooled = new List<GameObject>();
    private int amountPool = 10;
    [SerializeField] private GameObject Bala;
    private void Awake()
    {
        for (int i = 0; i < amountPool; i++)
        {
            GameObject obj = Instantiate(Bala, transform);
            obj.SetActive(false);
            pooled.Add(obj);
        }
        //this.gameObject.GetComponentInChildren<TextMeshProUGUI>()?.gameObject.SetActive(false);
    }
    /*public void OnMouseOver()
    {
        Debug.Log("Over");
        this.gameObject.GetComponentInChildren<TextMeshProUGUI>()?.gameObject.SetActive(true);
    }

    public void OnMouseExit()
    {
        Debug.Log("Exit");
        this.gameObject.GetComponentInChildren<TextMeshProUGUI>()?.gameObject.SetActive(false);
    }*/
    private void OnTriggerEnter2D(Collider2D collision)
    {
        EnemigoController e = collision.gameObject.GetComponent<EnemigoController>();
        if (collision.tag == "Enemy" && ((!e.air) || air))
        {
            enemigos.Add(e);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            enemigos.Remove(collision.gameObject.GetComponent<EnemigoController>());
        }
    }
    public GameObject GetPooled()
    {
        for (int i = 0; i < pooled.Count; i++)
        {
            if (!pooled[i].activeInHierarchy)
            {
                return pooled[i];
            }
        }
        return null;
    }
    private void OnEnable()
    {
        atk = tp.atk;
        dist = tp.dist;
        cost = tp.cost;
        vel = tp.vel;
        air = tp.air;
        this.gameObject.GetComponent<CircleCollider2D>().radius = dist / tp.scale;
        StartCoroutine(Disparar());
        c = new Comparator();
    }
    private IEnumerator Disparar()
    {
        while(true)
        {
            yield return new WaitForSeconds(vel);
            if(enemigos.Count >= 1)
            {
                enemigos.Sort(c);
                GameObject obj = GetPooled();
                if (obj != null)
                {
                    obj.GetComponent<BalaController>().e = enemigos[0].gameObject;
                    obj.GetComponent<BalaController>().atk = atk;
                    obj.transform.position = this.transform.position;
                    obj.SetActive(true);
                }
            }
        }
    }
    public void MoreSpeed()
    {
        vel /= 2;
        StartCoroutine(LessSpeed());
    }
    public void MoreAttack()
    {
        atk *= 2;
        StartCoroutine(LessAttack());
    }
    public IEnumerator LessSpeed() 
    {
            yield return new WaitForSeconds(10);
            vel *= 2;
    }
    public IEnumerator LessAttack()
    {
        yield return new WaitForSeconds(10);
        atk /= 2;
    }
}